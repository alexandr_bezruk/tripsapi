package com.bezruk.service;

import com.bezruk.domain.Trip;
import com.bezruk.model.BranchAnalysisResult;
import com.bezruk.model.HasRank;
import com.bezruk.model.Node;
import com.bezruk.repository.HotelRepository;
import com.bezruk.repository.ImageRepository;
import com.bezruk.repository.TripsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Service
@RequiredArgsConstructor
public class TripsService {

    private final TripsRepository repository;
    private final ImageRepository imageRepository;
    private final HotelRepository hotelRepository;
    private final TripSpecificationBuilder specificationBuilder;

    public List<Trip> getTrips(String searchParam) {
        Specification<Trip> specification = specificationBuilder.buildSpecification(searchParam);
        return repository.findAll(specification);
    }


    public Trip getMostSuitableTrip(List<Node> nodes) {
        calculateRanks(nodes);
        nodes.forEach(this::calculateChildNodesRanks);
        nodes.forEach(this::setParentNodeForChild);

        ArrayDeque<Node> queue = new ArrayDeque<>();
        nodes.forEach(node -> fillInQueue(queue, node));


        List<Node> nodesWithTrips = getNodesWithTrips(queue);
        calculateRanksForTrips(nodesWithTrips);

        Map<Integer, Double> tripValueToTripId = performAnalysis(nodesWithTrips);
        Integer tripId = tripValueToTripId.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .findFirst()
                .get()
                .getKey();

        return repository.findById(tripId).orElse(new Trip());
    }

    private void fillInQueue(Queue<Node> queue, Node node) {
        if (!(node.getChildNodes() == null) && !node.getChildNodes().isEmpty()) {
            List<Node> childNodes = node.getChildNodes();
            childNodes.sort(Comparator.comparing(Node::getOrder));

            childNodes.forEach(queue::offer);
            childNodes.forEach(childNode -> fillInQueue(queue, childNode));
        }
    }

    private List<Node> getNodesWithTrips(Queue<Node> queue) {
        List<Node> nodesWithTrips = new ArrayList<>();

        Node nodeFromQueue = queue.poll();
        while (nodeFromQueue != null || !queue.isEmpty()) {
            if (isNotEmpty(nodeFromQueue.getTrips())) {
                nodesWithTrips.add(nodeFromQueue);
            }
            nodeFromQueue = queue.poll();
        }

        return nodesWithTrips;
    }

    private boolean isNotEmpty(List list) {
        return list != null && !list.isEmpty();
    }

    private Map<Integer, Double> performAnalysis(List<Node> nodes) {
       List<BranchAnalysisResult> branchResults = new ArrayList<>();

        nodes.forEach(node -> {
            node.getTrips().forEach(trip -> {
                BranchAnalysisResult branchResult = calculateTripValue(trip, node);
                branchResults.add(branchResult);
            });
        });

        return branchResults.stream()
                .collect(Collectors.toMap((BranchAnalysisResult::getTripId), (BranchAnalysisResult::getValue), Double::sum));
    }

    private BranchAnalysisResult calculateTripValue(Trip trip, Node node) {
        double tripValue = 1.0;
        tripValue = tripValue * node.getRank() * trip.getRank();

        Node parentNode = node.getParentNode();

        while (parentNode != null) {
            tripValue = tripValue * parentNode.getRank();
            parentNode = parentNode.getParentNode();
        }

        BranchAnalysisResult branchResult = new BranchAnalysisResult();
        branchResult.setValue(tripValue);
        branchResult.setTripId(trip.getId());
        branchResult.setContextNode(node);
        return branchResult;
    }

    private void calculateRanksForTrips(List<Node> nodes) {
        nodes.forEach(node -> calculateRanks(node.getTrips()));
    }

    private void setParentNodeForChild(Node parentNode) {
        if (parentNode != null && isNotEmpty(parentNode.getChildNodes())) {
            parentNode.getChildNodes().forEach(node -> node.setParentNode(parentNode));
            parentNode.getChildNodes().forEach(this::setParentNodeForChild);
        }
    }

    private void calculateChildNodesRanks(Node node) {
        List<Node> childNodes = node.getChildNodes();
        if (childNodes != null && !childNodes.isEmpty()) {
            calculateRanks(childNodes);
            childNodes.forEach(this::calculateChildNodesRanks);
        }
    }

    private void calculateRanks(List<? extends HasRank> itemsWithRank) {
        int size = itemsWithRank.size();
        double coefficient = calculateCoefficient(itemsWithRank);
        for (int i = 0; i < itemsWithRank.size(); i++) {
            HasRank itemWithRank = itemsWithRank.get(i);
            int importance = (size - i);
            itemWithRank.setRank(coefficient * importance);
        }
    }

    private double calculateCoefficient(List<? extends HasRank> nodes) {
        double importanceSum = IntStream.range(0, nodes.size())
                .asDoubleStream()
                .map(value -> value + 1)
                .sum();

        return 1.0 / importanceSum;
    }

}
