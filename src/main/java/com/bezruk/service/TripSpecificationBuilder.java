package com.bezruk.service;

import com.bezruk.domain.Trip;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class TripSpecificationBuilder {


    public Specification<Trip> buildSpecification(String searchParam) {
        String[] fieldNameAndValue = searchParam.split("==");
        String name = fieldNameAndValue[0];
        String value = fieldNameAndValue[1];

        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(name).get("name"), value));
    }
}
