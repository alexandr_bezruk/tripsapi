package com.bezruk.service;

import com.bezruk.domain.City;
import com.bezruk.repository.CityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CityService {

    private final CityRepository repository;

    public List<City> getAllForCountry(Integer countryId) {
        return repository.findAllByCountryId(countryId);
    }

    public List<City> findAll() {
        return repository.findAll();
    }
}
