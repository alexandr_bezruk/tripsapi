package com.bezruk.service;

import com.bezruk.domain.Metadata;
import com.bezruk.repository.MetadataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MetadataService {

    private final MetadataRepository repository;


    public List<Metadata> getAll() {
        return repository.findAll();
    }

    public List<Metadata> getByKey(String key) {
        return repository.findAllByKey(key);
    }
}
