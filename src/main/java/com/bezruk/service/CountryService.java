package com.bezruk.service;

import com.bezruk.domain.Country;
import com.bezruk.repository.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CountryService {

    private final CountryRepository repository;

    public List<Country> getAll() {
        return repository.findAll();
    }

    public List<Country> getAllByResortType(String resortType) {
        return repository.findAllByResortTypesContains(resortType).stream()
                .distinct()
                .collect(Collectors.toList());
    }
}
