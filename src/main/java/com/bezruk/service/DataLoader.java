package com.bezruk.service;

import com.bezruk.domain.City;
import com.bezruk.domain.Country;
import com.bezruk.domain.Hotel;
import com.bezruk.domain.Trip;
import com.bezruk.repository.CityRepository;
import com.bezruk.repository.CountryRepository;
import com.bezruk.repository.HotelRepository;
import com.bezruk.repository.TripsRepository;
import liquibase.util.StreamUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Component
public class DataLoader {

    private final TripsRepository repository;
    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;
    private final HotelRepository hotelRepository;

        public void loadData() throws IOException {
        InputStream inputStream = new ClassPathResource("migrations/data/tripsukr.csv").getInputStream();
        String tripsString = StreamUtil.getStreamContents(inputStream);

        List<String> trips = Arrays.asList(tripsString.split("\n"));

        trips.forEach(trip -> {
            Country country = getCountry(trip);
            if (!countryRepository.existsByName(country.getName())) {
                country = countryRepository.save(country);
            } else {
                country = countryRepository.findByName(country.getName());
            }

            City city = getCity(trip);
            city.setCountryId(country.getId());

            if (!cityRepository.existsByName(city.getName())) {
                city = cityRepository.save(city);
            } else {
                city = cityRepository.findByName(city.getName());
            }

            Hotel hotel = getHotel(trip);
            hotel.setCityId(city.getId());

            hotel = hotelRepository.save(hotel);

            Trip tripToSave = new Trip();
            tripToSave.setCity(city);
            tripToSave.setCountry(country);
            tripToSave.setHotel(hotel);

            String duration = trip.split(",")[5];
            duration = duration.replace("дней", "днів");
            tripToSave.setDuration(duration);

            String cost = trip.split(",")[4];
            tripToSave.setCost(cost);

            String resortType = trip.split(",")[2];
            if (resortType.equalsIgnoreCase("Отдых на море")) {
                resortType = "SEASIDE_RESORT";
            } else {
                resortType = "SKI_RESORT";
            }
            tripToSave.setResortType(resortType);

            String transport = trip.split(",")[6];
            if (transport.equalsIgnoreCase("самолет")) {
                transport = "Літак";
            } else {
                transport = "Автобус";
            }
            tripToSave.setTransport(transport);

            repository.save(tripToSave);
        });
    }

    private Hotel getHotel(String trip) {
        String name = trip.split(",")[3];
        Hotel hotel = new Hotel();
        hotel.setName(name);
        return hotel;
    }

    private City getCity(String trip) {
        String name = trip.split(",")[0];
        City city = new City();
        city.setName(name);

        return city;
    }

    private Country getCountry(String trip) {
        String countryName = trip.split(",")[1];
        Country country = new Country();
        country.setName(countryName);

        return country;
    }

}
