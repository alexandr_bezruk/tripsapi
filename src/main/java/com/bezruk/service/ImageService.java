package com.bezruk.service;

import com.bezruk.domain.Image;
import com.bezruk.repository.ImageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class ImageService {

    private final ImageRepository imageRepository;

    public byte[] getImage(Integer id) throws IOException {
        Image image = imageRepository.findById(id).orElse(null);
        if (image != null) {
            return readFile(image);
        }
        return new byte[0];
    }

    private byte[] readFile(Image image) throws IOException {
        ClassPathResource resource = new ClassPathResource(image.getPath());
        return StreamUtils.copyToByteArray(resource.getInputStream());
    }
}
