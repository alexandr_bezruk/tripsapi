package com.bezruk.controller;

import com.bezruk.domain.City;
import com.bezruk.service.CityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class CityController {

    private final CityService service;

    @GetMapping("/countries/{countryId}/cities")
    public ResponseEntity<List<City>> getCitiesForCountry(@PathVariable(value = "countryId") Integer countryId) {
        return ResponseEntity.ok(service.getAllForCountry(countryId));
    }

    @GetMapping("/cities")
    public ResponseEntity<List<City>> getAll() {
        return ResponseEntity.ok(service.findAll());
    }
}
