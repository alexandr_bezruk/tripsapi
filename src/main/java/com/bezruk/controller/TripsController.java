package com.bezruk.controller;

import com.bezruk.domain.Trip;
import com.bezruk.model.Node;
import com.bezruk.service.TripsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class TripsController {

    private final TripsService tripsService;

    @PostMapping("/trips")
    public ResponseEntity<Trip> suggestTrip(@RequestBody List<Node> nodes) {
        return ResponseEntity.ok(tripsService.getMostSuitableTrip(nodes));
    }

    @GetMapping("/trips")
    public ResponseEntity<List<Trip>> getTrips(@RequestParam String search) {
        return ResponseEntity.ok(tripsService.getTrips(search));
    }
}
