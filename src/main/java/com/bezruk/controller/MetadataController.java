package com.bezruk.controller;

import com.bezruk.domain.Metadata;
import com.bezruk.service.MetadataService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MetadataController {

    private final MetadataService service;

    @GetMapping("/metadata")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<Metadata>> getAll() {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping(value = "/metadata/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<Metadata>> getAllByKey(@PathVariable String key) {
        return ResponseEntity.ok(service.getByKey(key));
    }

}
