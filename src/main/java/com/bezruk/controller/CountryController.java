package com.bezruk.controller;

import com.bezruk.domain.Country;
import com.bezruk.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class CountryController {

    private final CountryService service;

    @GetMapping("/countries")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<Country>> getAll() {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("resort-types/{resortType}/countries")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<Country>> getAllByResortType(@PathVariable String resortType) {
        return ResponseEntity.ok(service.getAllByResortType(resortType));
    }
}
