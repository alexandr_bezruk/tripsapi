package com.bezruk.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.bezruk.domain.Trip;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Node implements HasRank {

    private Integer entityId;
    private String type;
    private String value;
    private Integer order;
    private List<Node> childNodes = new ArrayList<>();
    private List<Trip> trips;

    @JsonIgnore
    private double rank;

    @JsonIgnore
    private Node parentNode;

}
