package com.bezruk.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BranchAnalysisResult {

    private Integer tripId;
    private double value;
    private Node contextNode;

}
