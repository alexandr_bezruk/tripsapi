package com.bezruk.model;

public interface HasRank {

    double getRank();

    void setRank(double rank);
}
