package com.bezruk.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "hotel")
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private Integer cityId;
    @OneToOne
    @JoinColumn(name = "image_id")
    private Image image;

//    @Transient
//    private Integer stars;
//
//    public Integer getStars() {
//        int starIndex = name.indexOf("*");
//        String starValue = name.substring(starIndex - 1, starIndex);
//        return Integer.parseInt(starValue);
//    }
}
