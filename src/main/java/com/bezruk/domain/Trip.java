package com.bezruk.domain;

import com.bezruk.model.HasRank;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "trips")
@Getter
@Setter
public class Trip implements HasRank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "city_id")
    private City city;
    @OneToOne
    @JoinColumn(name = "country_id")
    private Country country;
    @OneToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @Column(name = "resort_type")
    private String resortType;
    @Column
    private String cost;
    @Column
    private String duration;
    @Column
    private String transport;
    @Transient
    private double rank;
}
