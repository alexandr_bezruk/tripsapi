package com.bezruk.repository;

import com.bezruk.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountryRepository extends JpaRepository<Country, String> {

    @Query("SELECT c FROM Country c JOIN c.resortTypes types WHERE :resortType in (types)")
    List<Country> findAllByResortTypesContains(@Param("resortType") String resortType);

    boolean existsByName(String name);

    Country findByName(String name);

}
