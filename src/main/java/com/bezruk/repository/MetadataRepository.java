package com.bezruk.repository;

import com.bezruk.domain.Metadata;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MetadataRepository extends JpaRepository<Metadata, Integer> {

    List<Metadata> findAllByKey(String key);
}
