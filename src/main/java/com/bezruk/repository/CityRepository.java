package com.bezruk.repository;

import com.bezruk.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City, String> {

    List<City> findAllByCountryId(Integer countryId);

    boolean existsByName(String name);

    City findByName(String name);
}
