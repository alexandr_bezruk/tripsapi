package com.bezruk.repository;

import com.bezruk.domain.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface TripsRepository extends JpaRepository<Trip, Integer>, JpaSpecificationExecutor<Trip> {

    List<Trip> findAll();
}
