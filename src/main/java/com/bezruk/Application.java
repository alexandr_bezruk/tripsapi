package com.bezruk;


import com.bezruk.configuration.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SwaggerConfiguration.class)
public class Application {

    public static void main(String[] args) throws Exception {
        new SpringApplication(Application.class).run(args);
    }
}
